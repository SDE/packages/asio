Name: asio
Version: 1.24.0
Release: 1%{?dist}
Summary: Asio C++ Library

License: Boost
%define github github.com
%define gh_org FairRootGroup
%define gh_repo asio
URL: https://think-async.com/Asio
Source0: https://%{github}/%{gh_org}/%{gh_repo}/archive/v%{version}.tar.gz

BuildRequires: cmake
BuildRequires: gcc-c++

%description
Asio is a cross-platform C++ library for network and low-level I/O programming
that provides developers with a consistent asynchronous model using a modern
C++ approach.

%global debug_package %{nil}

%prep
%autosetup

%build
%define builddir build
cmake -S. -B%{builddir} \
      -DCMAKE_INSTALL_PREFIX=%{_prefix} \
      -DCMAKE_BUILD_TYPE=Release
cmake --build %{builddir} %{?_smp_mflags}

%install
DESTDIR=%{buildroot} cmake --build build --target install
echo "See %{url}/asio-%{version}/doc/" > README


%package devel
Summary: Development files for asio
Provides: asio-static = %{version}-%{release}

%description devel
This package contains the header files and CMake package for developing
against asio.

%files devel
%doc README
%license %{_datadir}/%{name}/LICENSE_1_0.txt
%license %{_datadir}/%{name}/COPYING
%{_includedir}/asio.hpp
%{_includedir}/%{name}
%dir %{_datadir}/cmake
%dir %{_datadir}/cmake/%{name}-%{version}
%{_datadir}/cmake/%{name}-%{version}/%{name}*.cmake


%changelog
* Fri Sep 23 2022 Dennis Klein <d.klein@gsi.de> - 1.24.0-1
- Package 1.24.0
* Thu Sep 9 2021 Dennis Klein <d.klein@gsi.de> - 1.19.2-1
- Package 1.19.2
* Tue Jul 13 2021 Dennis Klein <d.klein@gsi.de> - 1.19.1-1
- Package 1.19.1
* Wed May 12 2021 Dennis Klein <d.klein@gsi.de> - 1.18.1-1
- Package 1.18.1
